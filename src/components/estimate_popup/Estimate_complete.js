import React, { Component } from "react";
import Estimate_Form from "./Estimate_form";
import $ from "jquery";

export default class Estimate_complete extends Component {
  //Handle Schedule Appointment Popup Hide
  handleClose = () => {
    $("#schedule_appointment").hide();
  };

  //Render the view
  render() {
    return (
      <div>
        <div className="close_button">
          <button
            type="button"
            className="yes close"
            aria-label="Close"
            onClick={this.handleClose}
          >
            <span aria-hidden="true" className="close_button_text">
              close &times;
            </span>
          </button>
        </div>
        <Estimate_Form />
      </div>
    );
  }
}
