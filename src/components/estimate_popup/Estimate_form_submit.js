import React, { Component } from "react";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import TimePicker from "react-bootstrap-time-picker";
import callcenter from "../../assets/images/call_image.PNG";
import { addDays } from "date-fns";
import "../estimate_popup/Estimate_form.css";
import $ from "jquery";
import Estimate_form_two from "./Estimate_form_two";

export default class Estimate_form_submit extends Component {
  constructor(props) {
    super();
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.state = {
      startDate: null,
      Starttime: 0,
    };
  }
  //Handle DateChange and setState Initially
  handleDate = (date) => {
    this.setState({
      startDate: date,
    });
  };
  //Handle TimeChange and setState Initially
  handleTimeChange(time) {
    this.setState({
      Starttime: time,
    });
  }
  //Handle Thankyou App Popup Show Button with preventDefault func
  handleSubmitApp = (e) => {
    e.preventDefault();
    $("#thank_app").show();
  };

  //Render the view
  render() {
    return (
      <div>
        <div id="next_form">
          <div className="row">
            <div className="col-md-6 call_popup">
              <div className="leftside">
                <img
                  src={callcenter}
                  className="caller_img"
                  alt="Caller_Image"
                />
              </div>
            </div>
            <div className="col-md-6 right_formside">
              <div>
                <div className="formside_style">
                <div className="rightside_text">
                  THE CARPET GUYS APPT
                  <span className="rightside_textbelow">
                    we bring the store to your door!
                  </span>
                </div>
                  <Estimate_form_two />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
