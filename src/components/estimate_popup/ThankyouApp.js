import React, { Component } from "react";
import greenRoundTick from "../../assets/images/green_roundTick.png";
import $ from "jquery";

export default class ThankyouApp extends Component {
  //Handle Thankyou App Popup Hide
  handleThankyouApp = () => {
    $("#thank_app").hide();
  };

  //Render the view
  render() {
    return (
      <div className="thank-you-pop">
        <button type="button" className="close" onClick={this.handleThankyouApp}>
          <span>×</span>
        </button>
        <img src={greenRoundTick} alt="" />
        <h1>Thank You!</h1>
        <div className="call_title">WE RECIEVED YOUR APPT REQUEST</div>
        <p>
          We will be contacting you by email and phone to confirm your
          appointment. We look forward to helping you with all your flooring
          needs.
        </p>
      </div>
    );
  }
}
