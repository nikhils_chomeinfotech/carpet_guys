import React, { Component } from "react";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import TimePicker from "react-bootstrap-time-picker";
import { addDays } from "date-fns";
import "./Estimate_form.css";


const listeners = new Set();

export default class Estimate_date_time extends Component {
    componentDidUpdate(prevProps, prevState) {
        localStorage.setItem('current_date',this.state.startDate);
        localStorage.setItem('current_time',this.state.Starttime);
    }
    constructor(props) {
        super();
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.state = {
          startDate: null,
          Starttime: 39600,
          Initiatetime:"11:00",
          Endtime:"19:00",
          Interval:120,
          disable:true,
          CurrentDate:null,
          minStartDate: new Date(),
        };
      }
      componentDidMount() {
        let date = new Date();
        if(date.getHours() > 13){
            //show after noon times If selected before noon
            this.setState({
                minStartDate: addDays(new Date(), 1),
            });
        }
      }
     getNewRandomNum = () => {
        this.setState(
          prevState => {
            return {
              CurrentDate: prevState.startDate
            };
          },
        );
        this.managetime()
      };
      
        //Handle DateChange and setState Initially
        handleDate = (dates) => {
            this.setState({
                startDate: dates,
            });
            this.getNewRandomNum()
        };

       //Handle Time Management
       managetime() {
        var date = new Date();
        //If selected same day then show according to time
        if(this.state.startDate == new Date())
        {
            if(date.getHours() < 13){
            //show after noon times If selected before noon
                this.initiateTimeChange();
            }else{
                //show no time slots available for today
                this.disabletime();
            }
        }else{
            //If not selected same day
            this.initiateOldTime();
        }
      }
       //Handle Disabled time 
       disabletime() {
        this.setState({
            disable:true,
        });
      }
      //Handle TimeChange and setState Initially
      handleTimeChange(time) {
        this.setState({
          Starttime: time,
        });
      }
       //Handle TimeChange and setState Initially
       initiateTimeChange() {
        this.setState({
            disable:false,
            Initiatetime: "15:00",
            Endtime:"19:00",
            Interval:120,
        });
      }
       //Handle TimeChange and setState Initially
       initiateOldTime() {
        this.setState({
            disable:false,
            Initiatetime: "11:00",
            Endtime:"19:00",
            Interval:120,
        });
      }
  render() {
    return (
      <div>
        <div className="date_text">
          <label htmlFor="exampleInputEmail">Date: </label>
        </div>
        <div className="datepicker_style">
          <DatePicker
            selected={this.state.startDate}
            minDate={this.state.minStartDate}
            maxDate={addDays(new Date(), 6)}
            onChange={this.handleDate}
            placeholderText="Select a date"
            required
          />
        </div>
        <div className="time_text">
          <label htmlFor="exampleInputEmail">Time: </label>
        </div>
        <div className="timepicker_style">
          <TimePicker
            start={this.state.Initiatetime}
            end={this.state.Endtime}
            step={this.state.Interval}
            onChange={this.handleTimeChange}
            value={this.state.Starttime}
            required
            disabled={this.state.disable}
          />
        </div>
      </div>
    );
  }
}
