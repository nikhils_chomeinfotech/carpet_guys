import React from "react";
import {
    Formik,
    Field,
    Form,
    useField,
    FieldAttributes,
    FieldArray,
    ErrorMessage
  } from "formik";
  import {
    TextField,
    Button,
    Checkbox,
    Radio,
    FormControlLabel,
    Select,
    MenuItem,
    FormGroup,
    CheckboxProps
  } from "@material-ui/core";
import * as yup from "yup";
import $ from "jquery";
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import Estimate_date_time from "./Estimate_date_time";


type MyTextFieldProps = { label: string } & FieldAttributes<{}>;

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      },
  }));


  type MyRadioProps = { label: string } & FieldAttributes<{}>;

  const MyRadio: React.FC<MyRadioProps> = ({ label, ...props }) => {
  const [field] = useField<{}>(props);
  return <FormControlLabel {...field} control={<Radio />} label={label} />;
};

const MyTextField: React.FC<MyTextFieldProps> = ({
  placeholder,
  label,
  ...props
}) => {
  const [field, meta] = useField<{}>(props);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <TextField
      placeholder={placeholder}
      {...field}
      helperText={errorText}
      error={!!errorText}
      variant="outlined"
      label={label}
    />
  );
};

const validationSchema = yup.object({
  build_before: yup
  .string().required("This field is required!")
});

const Estimate_form_two: React.FC = () => {
  const classes = useStyles();
  return (
    <div>
      <Formik
        validateOnChange={true}
        initialValues={{
          build_before:'',
          flooring_room:[],
          isSubmitting:"false",
          current_time:'',
          current_date:'',
          estimate_user_id : localStorage.getItem('estimate_user_id'),
        }}
        validationSchema={validationSchema}
        onSubmit={(data, { setSubmitting, resetForm }) => {
          setSubmitting(true); 
          $("#backdrop_estimate_div_submit").show();
          // make async call
          if(data){
            console.log(data);
            
            localStorage.setItem('build_before',data.build_before);
            // localStorage.setItem('current_date',data.flooring_room);
          fetch('http://54.188.196.248/carpet_guys/public/index.php/api/send_estimate_form_two',{
            method : "POST",
            headers: {
                "Accept"        : "application/json",
                "Content-Type"  : "application/json"
            },
            body:JSON.stringify(data)
        }).then((result)=>{
            result.json().then((response)=> {
              console.log(response);
            if(response.data){
              $("#backdrop_estimate_div_submit").hide();
              $("#next_form").hide();
              $("#prev_form").show();
              $("#thank_app").show();
              resetForm({})
            }
          });
        });
      }else{
        console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
      }
      setSubmitting(false);
        }}
      >
        {({ values, errors, isSubmitting }) => (
          <Form translate="yes">
            <div className="floor_text_next">
             <label>Was your home build before 1978?*</label>
             <FormGroup className="build_before_radio">
             <MyRadio
              name="build_before"
              type="radio"
              value="Yes"
              label="Yes"
              className="build_before_radio_margin"
            />
             <MyRadio
              name="build_before"
              type="radio"
              value="No"
              label="No"
              className="build_before_radio_margin"
            />
             <MyRadio
              name="build_before"
              type="radio"
              value="Unknown"
              label="Unknown"
              className="build_before_radio_margin"
            />
             </FormGroup>
             <div className="error_message">
               <ErrorMessage name="build_before"/>
             </div>
             </div>
             <div className="floor_text_next">
             <label>Select Flooring we can help you with</label>
                <FormGroup className="multiple_dropStyle">
                  <MyCheckbox
                    name="flooring_room"
                    value="LIVING ROOM"
                    label="LIVING ROOM"
                  />
                  <MyCheckbox
                    name="flooring_room"
                    value="BATHROOM(S)"
                    label="BATHROOM(S)"
                  />
                  <MyCheckbox
                    name="flooring_room"
                    value="BASEMENT"
                    label="BASEMENT"
                  />

                  <MyCheckbox
                    name="flooring_room"
                    value="BEDROOM(S)"
                    label="BEDROOM(S)"
                  />
                   <MyCheckbox
                    name="flooring_room"
                    value="KITCHEN"
                    label="KITCHEN"
                  />
                  <MyCheckbox
                    name="flooring_room"
                    value="STAIRS"
                    label="STAIRS"
                  />
                  <MyCheckbox
                    name="flooring_room"
                    value="OTHER"
                    label="OTHER"
                  />
                </FormGroup>
             </div>
             <div className="floor_text_next">
             <label>PLEASE SELECT DATE & TIME THAT IS GOOD FOR US TO COME TO YOUR HOME?*</label>
                <Estimate_date_time />
             </div>
             {/* <TextField name="current_time" id="current_time" hidden={true} />
             <TextField name="current_date" id="current_date" hidden={true}/> */}
            <div className="estimate_formButton">
              <Button disabled={isSubmitting} type="submit">
                SUBMIT
              </Button>
            </div>
             {/*<pre>{JSON.stringify(errors, null, 2)}</pre> */}
          </Form>
        )}
      </Formik>
      <div id="backdrop_estimate_div_submit">
      <Backdrop className={classes.backdrop} open={true}>
        <CircularProgress color="inherit" />
      </Backdrop>
      </div>
    </div>
  );
};

export interface MyCheckboxProps extends CheckboxProps {
  name: string;
  value?: string | number;
  label?: string;
}

export function MyCheckbox(props: MyCheckboxProps) {
  const [field] = useField({
    name: props.name,
    type: 'checkbox',
    value: props.value
  });
  return (
    <FormControlLabel
      control={<Checkbox {...props} {...field} />}
      label={props.label}
    />
  );
}
export default Estimate_form_two;
