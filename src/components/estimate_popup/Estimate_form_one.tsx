import React from "react";
import {
    Formik,
    Field,
    Form,
    useField,
    FieldAttributes,
    FieldArray
  } from "formik";
  import {
    TextField,
    Button,
    Checkbox,
    Radio,
    FormControlLabel,
    Select,
    MenuItem,
    FormGroup,
    CheckboxProps
  } from "@material-ui/core";
import * as yup from "yup";
import $ from "jquery";
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';


type MyTextFieldProps = { label: string } & FieldAttributes<{}>;

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      },
  }));

const MyTextField: React.FC<MyTextFieldProps> = ({
  placeholder,
  label,
  ...props
}) => {
  const [field, meta] = useField<{}>(props);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <TextField
      placeholder={placeholder}
      {...field}
      helperText={errorText}
      error={!!errorText}
      variant="outlined"
      label={label}
    />
  );
};
const phoneRegExp = /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/

const validationSchema = yup.object({
  first_name: yup
    .string()
    .required("Enter first name")
    .max(100,"First Name must be 100"),
  last_name: yup
    .string()
    .required("Enter last name")
    .max(100),
  phone_number : yup
  .string()
  .matches(phoneRegExp, "Phone number is not valid")
  .required("Enter a valid phone number"),
  email : yup
  .string()
  .email("Email address is not valid")
  .required("Enter a valid email address"),
  address: yup
  .string()
  .required("Enter a valid address"),
  zip: yup
  .string()
  .required("Enter a valid zipcode"),
});

const Estimate_form_one: React.FC = () => {
  const classes = useStyles();
  return (
    <div>
      <Formik
        validateOnChange={true}
        initialValues={{
          first_name: "",
          last_name: "",
          phone_number:"",
          email:"",
          address:"",
          zip:"",
          state: 0,
          city: 0,
          flooring_check:[],
          isSubmitting:"false"
        }}
        validationSchema={validationSchema}
        onSubmit={(data, { setSubmitting, resetForm }) => {
          setSubmitting(true); 
          $("#backdrop_estimate_div").show();
          // make async call
          if(data){
          fetch('http://54.188.196.248/carpet_guys/public/index.php/api/send_estimate_form_one',{
            method : "POST",
            headers: {
                "Accept"        : "application/json",
                "Content-Type"  : "application/json"
            },
            body:JSON.stringify(data)
        }).then((result)=>{
            result.json().then((response)=> {
              console.log(response);
            if(response.data){
              localStorage.setItem('estimate_user_id',response.data);
              $("#backdrop_estimate_div").hide();
              $("#prev_form").hide();
              $("#next_form").show();
              resetForm({})
            }
          });
        });
      }else{
        console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
      }
      setSubmitting(false);
        }}
      >
        {({ values, errors, isSubmitting }) => (
          <Form translate="yes">
             <div className="form_inputs_estimate">
                <MyTextField  name="first_name" id="first-name" label="First Name*"/>
             </div>
             <div className="form_inputs_estimate">
                <MyTextField  name="last_name" id="last-name" label="Last Name*"/>
             </div>
             <div className="form_inputs_estimate">
                <MyTextField  name="phone_number" id="phone-number" label="Phone Number*"/>
             </div>
             <div className="form_inputs_estimate">
                <MyTextField  name="email" id="email" label="Email*"/>
             </div>
             <div className="form_inputs_estimate">
                <MyTextField  name="address" id="address" label="Address*"/>
             </div>
             <div className="form_inputs_estimate">
                <MyTextField  name="zip" id="zip" label="Zip*"/>
             </div>
             <div className="form_inputs_estimate">
             <Field
                name="state"
                label="State"
                as={TextField}
                select
              >
                <MenuItem value={0}>Select ...</MenuItem>
                <MenuItem value="California">California</MenuItem>
                <MenuItem value="New York">New York</MenuItem>
                <MenuItem value="Ohio">Ohio</MenuItem>
              </Field>
             </div>
             <div className="form_inputs_estimate">
             <Field
                    name="city"
                    label="City"
                    as={TextField}
                    select
                  >
                <MenuItem value={0}>Select ...</MenuItem>
                <MenuItem value="Los Angeles">Los Angeles</MenuItem>
                <MenuItem value="New York">New York</MenuItem>
                <MenuItem value="Columbus">Columbus</MenuItem>
                  </Field>
             </div>
             <div className="floor_text">
             <label>Select Flooring we can help you with</label>
                <FormGroup className="multiple_dropStyle">
                  <MyCheckbox
                    name="flooring_check"
                    value="Carpet"
                    label="Carpet"
                  />
                  <MyCheckbox
                    name="flooring_check"
                    value="Laminate"
                    label="Laminate"
                  />
                  <MyCheckbox
                    name="flooring_check"
                    value="Vinyl"
                    label="Vinyl"
                  />
                  <MyCheckbox
                    name="flooring_check"
                    value="Hardwood"
                    label="Hardwood"
                  />
                </FormGroup>
             </div>
            <div className="estimate_formButton">
              <Button disabled={isSubmitting} type="submit">
                NEXT
              </Button>
            </div>
            {/* <pre>{JSON.stringify(values, null, 2)}</pre> */}
             {/*<pre>{JSON.stringify(errors, null, 2)}</pre> */}
          </Form>
        )}
      </Formik>
      <div id="backdrop_estimate_div">
      <Backdrop className={classes.backdrop} open={true}>
        <CircularProgress color="inherit" />
      </Backdrop>
      </div>
    </div>
  );
};

export interface MyCheckboxProps extends CheckboxProps {
  name: string;
  value?: string | number;
  label?: string;
}

export function MyCheckbox(props: MyCheckboxProps) {
  const [field] = useField({
    name: props.name,
    type: 'checkbox',
    value: props.value
  });
  return (
    <FormControlLabel
      control={<Checkbox {...props} {...field} />}
      label={props.label}
    />
  );
}
export default Estimate_form_one;
