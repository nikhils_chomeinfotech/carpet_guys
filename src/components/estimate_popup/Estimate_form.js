import React, { Component } from "react";
import "../estimate_popup/Estimate_form.css";
import callcenter from "../../assets/images/call_image.PNG";
import Estimate_form_submit from "./Estimate_form_submit";
import Thankyou_App from "./ThankyouApp";
import $ from "jquery";
import Estimate_form_one from "./Estimate_form_one";

export default class Estimate_form extends Component {
  //Handle Show Form Step 2 and Hide Step 1 with preventDefault func
  handleNext = (e) => {
    e.preventDefault();
    $("#prev_form").hide();
    $("#next_form").show();
  };

  //render the view
  render() {
    return (
      <div>
        <div id="prev_form">
          <div className="row">
            <div className="col-6 call_popup">
              <div className="leftside">
                <img
                  src={callcenter}
                  className="caller_img"
                  alt="Caller_Image"
                />
              </div>
            </div>
            <div className="col-6 right_formside">
              <div>
                <div className="formside_style">
                  <div className="rightside_text">
                    THE CARPET GUYS APPT
                    <span className="rightside_textbelow">
                      we bring the store to your door!
                    </span>
                  </div>
                  <div>
                    <Estimate_form_one />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="next_form">
          <Estimate_form_submit />
        </div>
        {/* Thank You popup Start*/}
        <div id="thank_app">
          <Thankyou_App />
        </div>
        {/* Thank You popup End*/}
      </div>
    );
  }
}

$("#thank_app").hide();
$(document).ready(function() {
  $("#next_form").hide();
});
