import React, { Component } from "react";
import callcenter from "../../assets/images/call_image.PNG";
import $ from "jquery";
import Calling_validation from './CallingValidation'

export default class Calling_form extends Component {
  //Handle Call Popup Close Button
  handleCallClose = () => {
    $("#calling_popup").hide();
  };
  //Handle Thankyou Popup Show Button with preventDefault func
  handleThankyou = (e) => {
    e.preventDefault();
    $("#thankyou").show();
  };
  //render the view
  render() {
    return (
      <div>
        <div className="close_button">
          <button
            type="button"
            className="yes close"
            aria-label="Close"
            onClick={this.handleCallClose}
          >
            <span aria-hidden="true" className="close_button_text">
              close &times;
            </span>
          </button>
        </div>
        <div className="row">
          <div className="col-6 call_popup">
            <div className="leftside">
              <img src={callcenter} className="caller_img" alt="Caller_Image" />
            </div>
          </div>
          <div className="col-6 right_formside">
            <div className="formside_style">
              <div className="calling_rightside_text">
                CONTACT THE CARPET GUYS
                <span className="rightside_textbelow">
                  we bring the store to your door!
                </span>
              </div>
              <Calling_validation />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
