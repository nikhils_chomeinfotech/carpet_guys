import React, { Component } from "react";
import greenRoundTick from "../../assets/images/green_roundTick.png";
import $ from "jquery";

export default class Thankyou_form extends Component {
  //Handle Thankyou Popup Hide
  handleThankyouClose = () => {
    $("#thankyou").hide();
  };
  //Render the view
  render() {
    return (
      <div className="thank-you-pop">
        <button type="button" className="close" onClick={this.handleThankyouClose}>
          <span>×</span>
        </button>
        <img src={greenRoundTick} alt="" />
        <h1>Thank You!</h1>
        <div className="call_title">WE RECIEVED YOUR NUMBER</div>
        <p>
          Thank you for your request one of our Flooring Experts will contact
          you shortly. We look forward to helping you with your flooring needs.
        </p>
      </div>
    );
  }
}
