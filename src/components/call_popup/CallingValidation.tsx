import React from "react";
import {
  Formik,
  Form,
  useField,
  FieldAttributes,
} from "formik";
import {
  TextField,
  Button,
} from "@material-ui/core";
import * as yup from "yup";
import $ from "jquery";
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

type MyTextFieldProps = { label: string } & FieldAttributes<{}>;

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const MyTextField: React.FC<MyTextFieldProps> = ({
  placeholder,
  label,
  ...props
}) => {
  const [field, meta] = useField<{}>(props);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <TextField
      placeholder={placeholder}
      {...field}
      helperText={errorText}
      error={!!errorText}
      variant="outlined"
      label={label}
    />
  );
};
const phoneRegExp = /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/

const validationSchema = yup.object({
  first_name: yup
    .string()
    .required("Enter first name")
    .max(100),
  last_name: yup
    .string()
    .required("Enter last name")
    .max(100),
  phone_number : yup
  .string()
  .matches(phoneRegExp, "Phone number is not valid")
  .required("Enter a valid phone number")
});

const CallingValidation: React.FC = () => {
  const classes = useStyles();
  return (
    <div>
      <Formik
        validateOnChange={true}
        initialValues={{
          first_name: "",
          last_name: "",
          phone_number:"",
          isSubmitting:"false"
        }}
        validationSchema={validationSchema}
        onSubmit={(data, { setSubmitting, resetForm }) => {
          setSubmitting(true); 
          $("#backdrop_div").show();
          // make async call
          if(data){
          fetch('http://54.188.196.248/carpet_guys/public/index.php/api/send_contact_form',{
            method : "POST",
            headers: {
                "Accept"        : "application/json",
                "Content-Type"  : "application/json"
            },
            body:JSON.stringify(data)
        }).then((result)=>{
            result.json().then((response)=> {
            if(response.data){
              $("#backdrop_div").hide();
              $("#thankyou").show();
              resetForm({})
            }
          });
        });
      }else{
        console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
      }
      setSubmitting(false);
        }}
      >
        {({ values, errors, isSubmitting }) => (
          <Form translate="yes">
             <div className="form_inputs_call">
                <MyTextField  name="first_name" id="first-name" label="First Name*"/>
             </div>
             <div className="form_inputs_call">
                <MyTextField  name="last_name" id="last-name" label="Last Name*"/>
             </div>
             <div className="form_inputs_call">
                <MyTextField  name="phone_number" id="phone-number" label="Phone Number*"/>
             </div>
            <div className="call_formButton">
              <Button disabled={isSubmitting} type="submit">
                submit
              </Button>
            </div>
            {/* <pre>{JSON.stringify(values, null, 2)}</pre>
            <pre>{JSON.stringify(errors, null, 2)}</pre> */}
          </Form>
        )}
      </Formik>
      <div id="backdrop_div">
      <Backdrop className={classes.backdrop} open={true}>
        <CircularProgress color="inherit" />
      </Backdrop>
      </div>
    </div>
  );
};

export default CallingValidation;
