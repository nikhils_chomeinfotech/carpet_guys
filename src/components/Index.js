import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./call_popup/Calling_popup.css";
import Thankyou_form from "./call_popup/Thankyou_form";
import Calling_form from "./call_popup/Calling_form";
import Estimate_complete from "./estimate_popup/Estimate_complete";
import Welcome from "./Welcome";
import "../assets/script";

export default class Index extends Component {
//In this main component, All components are render one by one
  render() {
    return (
      <div>
        <h1 className="text-center">Welcome, User</h1>
        {/* Background Blur div Start*/}
        <div className="overlay_div" id="overlay">
          {/* Main popup Start*/}
          <div id="winner">
            <Welcome />
          </div>
          {/* Main popup End*/}
        </div>
        {/* Background Blur div End*/}
        {/* Call popup Start*/}
        <div id="calling_popup">
          <Calling_form />
        </div>
        {/* Call popup End*/}
        {/* Schedule Appointment popup Start*/}
        <div id="schedule_appointment">
          <Estimate_complete />
        </div>
        {/* Schedule Appointment popup End*/}
        {/* Thank You popup Start*/}
        <div id="thankyou">
          <Thankyou_form />
        </div>
        {/* Thank You popup End*/}
      </div>
    );
  }
}
