import React, { Component } from "react";
import SimpleForm from "./chatboat/Simple_form";
import callcenter from "../assets/images/call_image.PNG";
import phone from "../assets/images/phone.png";
import $ from "jquery";

export default class Welcome extends Component {
  //Handle Confirm and quit Button to redirect to home page
  handleClick = () => {
    $("#schedule_appointment").show();
  };
  //Handle Confirm and quit Button to redirect to home page
  handleCallShow = () => {
    $("#calling_popup").show();
  };

  render() {
    return (
      <div>
        <div className="close_button">
          <button type="button" className="yes close" aria-label="Close">
            <span aria-hidden="true" className="close_button_text">
              close &times;
            </span>
          </button>
        </div>

        <div className="row">
          <div className="col-6 call_popup">
            <div className="leftside">
              <div className="call_buttons">
                <button className="call_button" onClick={this.handleCallShow}>
                  <img src={phone} className="call_image_style" alt="phone_icon"/>
                  CLICK HERE TO CALL NOW!
                </button>
              </div>
              <img
                src={callcenter}
                className="caller_img"
                alt="Call_Center_Image"
              />
              <div className="call_buttons">
                <button className="schedule_button" onClick={this.handleClick}>
                  CLICK HERE TO SCHEDULE A FREE IN-HOME ESTIMATE
                </button>
              </div>
            </div>
          </div>

          <div className="col-6 chatside">
            <div>
              <SimpleForm></SimpleForm>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
