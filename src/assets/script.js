import $ from "jquery";

$("#schedule_appointment").hide();
$("#calling_popup").hide();
$("#thankyou").hide();
$("#schedule_appointment")
  .find(".yes")
  .unbind()
  .click(function() {
    $("#schedule_appointment").hide();
  });
//Here If mouse leave current tab then we show confirmation popup
// var mouseX = 0;
var mouseY = 0;
document.addEventListener("mousemove", function(e) {
  // mouseX = e.clientX;
  mouseY = e.clientY;
});
$(document).mousemove(function(msg, myYes) {
  if (mouseY < 100 || mouseY > 500) {
    msg = "You forgot something, are you sure you want to leave?";
    var confirmBox = $("#winner");
    confirmBox.find(".message").text(msg);
    confirmBox
      .find(".yes")
      .unbind()
      .click(function() {
        confirmBox.hide();
        $("#overlay").hide();
      });
    confirmBox.find(".yes").click(myYes);
    confirmBox.show();
    $("#overlay").show();
  }
});
